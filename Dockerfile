FROM python:3.9-slim

RUN addgroup --gid 1001 --system app && \
    adduser --no-create-home --shell /bin/false --disabled-password --uid 1001 --system --group app

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY src /home/app

USER app

ENTRYPOINT ["python"]
CMD ["/home/app/main.py"]
